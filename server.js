const express = require('express');
const app = express();

app.use(express.static(__dirname + '/dist/country-explorer/'));

app.all('*', (req, res) => {
	res.status(200).sendFile(__dirname + '/dist/country-explorer/index.html');
});

app.listen(process.env.PORT || 4200, () => {
	console.log(`Running on port ${process.env.PORT} or 4200`);
});
