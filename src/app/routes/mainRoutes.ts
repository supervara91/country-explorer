import { Routes } from '@angular/router';

import { CurrencyDetailsComponent } from '../components/description/currency-details/currency-details.component';
import { DescriptionDetailsComponent } from '../components/description/description-details/description-details.component';
import { NewsDetailsComponent } from '../components/description/news-details/news-details.component';
import { WeatherDetailsComponent } from '../components/description/weather-details/weather-details.component';

export const mainRoutes: Routes = [
	{ path: 'description', component: DescriptionDetailsComponent },
	{ path: 'news', component: NewsDetailsComponent },
	{ path: 'currency', component: CurrencyDetailsComponent },
	{ path: 'weather', component: WeatherDetailsComponent }
];
