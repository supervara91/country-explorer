import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {
	particlesStyle: object = {
		'width': '100%',
		'height': '200px',
		'margin-bottom': '-200px',
		'z-index': -1
	};
	// 'position': 'fixed',
	// 'top': 0,
	// 'left': 0,
	// 'right': 0,
	// 'bottom': 0,
	particlesParams: object = {
		particles: {
			number: {
				value: 100,
				density: {
					value_area: '500'
				}
			},
			color: {
				value: '#ffffff'
			},
			shape: {
				type: 'circle'
			},
			move: {
				speed: '3'
			}
		}
	};
}
