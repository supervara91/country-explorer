import { NgRedux, select } from '@angular-redux/store';
import { Injectable, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs';

import { LOAD_NEWS, LOADING_NEWS } from '../redux/actions';
import { IAppState } from '../redux/store';

@Injectable({
	providedIn: 'root'
})

export class NewsService implements OnDestroy {
	private api = 'https://newsapi.org/v2/top-headlines?apiKey=e9bfda99e53e45e89b374117d40fec22&country=';
	@select('country') country;
	subscriptionCountry: Subscription;
	subscriptionNews: Subscription;

	constructor(private http: Http, private ngRedux: NgRedux<IAppState>) { }

	loadNews() {
		this.subscriptionCountry = this.country.subscribe(country => {
			if (country.name) {
				this.ngRedux.dispatch({ type: LOADING_NEWS });
				this.subscriptionNews = this.http.get(this.api + country.alpha2Code).subscribe(res => {
					this.ngRedux.dispatch({ type: LOAD_NEWS, news: res.json().articles });
				});
			}
		});
	}

	ngOnDestroy() {
		this.subscriptionCountry.unsubscribe();
		this.subscriptionNews.unsubscribe();
	}
}
