import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class CountryService {
	private api = 'https://restcountries.eu/rest/v2/';

	constructor(private http: Http) { }

	getAll() {
		return this.http.get(this.api + 'all').pipe(
			map((res) => res.json())
		);
	}
}
