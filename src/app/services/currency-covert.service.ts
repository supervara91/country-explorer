import { NgRedux, select } from '@angular-redux/store';
import { Injectable, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs';

import { LOAD_CONVERSION, LOADING_CURRENCIES } from '../redux/actions';
import { IAppState } from '../redux/store';

@Injectable({
	providedIn: 'root'
})
export class CurrencyCovertService implements OnDestroy {
	@select('country') country;
	countryTmp;
	private api = 'https://free.currencyconverterapi.com/api/v6/convert?compact=ultra&q=';
	private subscriptionCountry: Subscription;
	private subscriptionCurrency: Subscription;
	private codeCoins = [];

	constructor(
		private http: Http,
		private ngRedux: NgRedux<IAppState>
	) { }

	loadCurrencies() {
		this.subscriptionCountry = this.country.subscribe(country => {
			if (country.name) {
				this.ngRedux.dispatch({ type: LOADING_CURRENCIES });
				this.countryTmp = country;
				this.addConversion(country.currencies);
			}
		});
	}

	addConversion(value) {
		value.forEach(currency => {
			this.codeCoins.push('USD_' + currency['code']);
		});
		this.getCurrencyConvert(this.codeCoins.toString());
	}

	getCurrencyConvert(codes) {
		console.log(this.api + codes);
		this.subscriptionCurrency = this.http.get(this.api + codes).subscribe(res => {
			const currencies = res.json();
			for (let i = 0; i < this.countryTmp.currencies.length; i++) {
				this.countryTmp.currencies[i].convert = currencies[this.codeCoins[i]];
			}
			this.ngRedux.dispatch({ type: LOAD_CONVERSION, currencies: this.countryTmp.currencies });
		});
	}

	ngOnDestroy() {
		this.subscriptionCountry.unsubscribe();
		this.subscriptionCurrency.unsubscribe();
	}
}
