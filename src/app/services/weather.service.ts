import { NgRedux, select } from '@angular-redux/store';
import { Injectable, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs';

import { LOAD_WEATHER, LOADING_WEATHER } from '../redux/actions';
import { IAppState } from '../redux/store';

@Injectable({
	providedIn: 'root'
})
export class WeatherService implements OnDestroy {
	@select('country') country;
	private api = 'https://api.openweathermap.org/data/2.5/weather?&APPID=81afa3805727ac9a76b4f04aa960aa3f&units=metric&';
	private subscriptionCountry: Subscription;
	private subscriptionWeather: Subscription;

	constructor(private http: Http, private ngRedux: NgRedux<IAppState>) { }

	loadWeather() {
		this.subscriptionCountry = this.country.subscribe(country => {
			if (country.name) {
				this.ngRedux.dispatch({ type: LOADING_WEATHER });
				this.http.get(this.api + 'lat=' + country.latlng[0] + '&lon=' + country.latlng[1]).subscribe(res => {
					this.ngRedux.dispatch({ type: LOAD_WEATHER, weather: res.json() });
				});
			}
		});
	}

	ngOnDestroy() {
		this.subscriptionCountry.unsubscribe();
		this.subscriptionWeather.unsubscribe();
	}
}
