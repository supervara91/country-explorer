import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { IAppState, INITIAL_STATE, rootReducer } from './store';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		NgReduxModule
	]
})
export class ReduxModule {

	constructor(private ngRedux: NgRedux<IAppState>) {
		ngRedux.configureStore(rootReducer, INITIAL_STATE);
	}
}
