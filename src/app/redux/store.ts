import { tassign } from 'tassign';

import { CHANGE_COUNTRY, LOAD_CONVERSION, LOAD_NEWS, LOAD_WEATHER, LOADING_CURRENCIES, LOADING_NEWS, LOADING_WEATHER } from './actions';

export interface IAppState {
	country: Object;
	center: {
		lat: number,
		lng: number
	};
	news: any[];
	currencies: any[];
	weather: Object;
	loadingNews: boolean;
	loadingCurrencies: boolean;
	loadingWeather: boolean;
}

export const INITIAL_STATE = {
	country: {},
	center: {
		lat: 21.5,
		lng: -80
	},
	news: [],
	currencies: [],
	weather: {},
	loadingNews: false,
	loadingCurrencies: false,
	loadingWeather: false
};

export function rootReducer(state: IAppState, action): IAppState {
	switch (action.type) {
		case LOADING_NEWS:
			return loadingNews(state, action);
		case LOADING_CURRENCIES:
			return loadingCurrencies(state, action);
		case LOADING_WEATHER:
			return loadingWeather(state, action);
		case CHANGE_COUNTRY:
			return changeCountry(state, action);
		case LOAD_NEWS:
			return loadNews(state, action);
		case LOAD_CONVERSION:
			return loadConversion(state, action);
		case LOAD_WEATHER:
			return loadWeather(state, action);
	}
	return state;
}

function changeCountry(state, action) {
	return tassign(state, {
		country: action.country,
		center: {
			lat: action.center[0],
			lng: action.center[1]
		},
		news: [],
		currencies: action.currencies,
		loading: false
	});
}

function loadNews(state, action) {
	return tassign(state, {
		news: action.news,
		loadingNews: false
	});
}


function loadConversion(state, action) {
	return tassign(state, {
		currencies: action.currencies,
		loadingCurrencies: false
	});
}

function loadWeather(state, action) {
	return tassign(state, {
		weather: action.weather,
		loadingWeather: false
	});
}

function loadingNews(state, action) {
	return tassign(state, {
		loadingNews: true
	});
}

function loadingCurrencies(state, action) {
	return tassign(state, {
		loadingCurrencies: true
	});
}

function loadingWeather(state, action) {
	return tassign(state, {
		loadingWeather: true
	});
}
