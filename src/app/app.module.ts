import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgSelectModule } from '@ng-select/ng-select';
import { ParticlesModule } from 'angular-particle';

import { AppComponent } from './app.component';
import { CurrencyDetailsComponent } from './components/description/currency-details/currency-details.component';
import { DescriptionDetailsComponent } from './components/description/description-details/description-details.component';
import { DescriptionComponent } from './components/description/description.component';
import { NewsDetailsComponent } from './components/description/news-details/news-details.component';
import { WeatherDetailsComponent } from './components/description/weather-details/weather-details.component';
import { LeafletMapComponent } from './components/leaflet-map/leaflet-map.component';
import { SelectCountryComponent } from './components/select-country/select-country.component';
import { ReduxModule } from './redux/redux.module';
import { AppRoutingModule } from './routes/app-routing.module';
import { CountryService } from './services/country.service';
import { CurrencyCovertService } from './services/currency-covert.service';
import { NewsService } from './services/news.service';
import { WeatherService } from './services/weather.service';

@NgModule({
	declarations: [
		AppComponent,
		SelectCountryComponent,
		LeafletMapComponent,
		DescriptionComponent,
		DescriptionDetailsComponent,
		NewsDetailsComponent,
		CurrencyDetailsComponent,
		WeatherDetailsComponent
	],
	imports: [
		AppRoutingModule,
		ReduxModule,
		HttpModule,
		BrowserModule,
		NgSelectModule,
		ParticlesModule,
		LeafletModule.forRoot()
	],
	providers: [
		CountryService,
		WeatherService,
		NewsService,
		CurrencyCovertService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
