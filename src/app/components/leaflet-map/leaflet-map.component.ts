import { select } from '@angular-redux/store';
import { Component } from '@angular/core';
import { latLng, tileLayer } from 'leaflet';

@Component({
	selector: 'leaflet-map-component',
	templateUrl: './leaflet-map.component.html',
	styleUrls: ['./leaflet-map.component.css']
})
export class LeafletMapComponent {
	@select('center') center;
	options = {
		layers: [
			tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
		],
		zoom: 5,
		center: latLng(21.5, -80)
	};

	constructor() { }

}
