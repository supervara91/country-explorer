import { NgRedux } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CHANGE_COUNTRY } from '../../redux/actions';
import { CountryService } from '../../services/country.service';
import { IAppState } from '../../redux/store';

@Component({
	selector: 'select-country-component',
	templateUrl: './select-country.component.html',
	styleUrls: ['./select-country.component.css']
})
export class SelectCountryComponent implements OnInit {
	countries$: Observable<any>;

	constructor(
		private countryService: CountryService,
		private ngRedux: NgRedux<IAppState>
	) { }

	ngOnInit() {
		this.countries$ = this.countryService.getAll();
	}

	select(value) {
		this.ngRedux.dispatch({ type: CHANGE_COUNTRY, country: value, center: value.latlng });
	}
}
