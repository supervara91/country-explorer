import { select } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { CurrencyCovertService } from 'src/app/services/currency-covert.service';

@Component({
	selector: 'currency-details-component',
	templateUrl: './currency-details.component.html',
	styleUrls: ['./currency-details.component.css']
})
export class CurrencyDetailsComponent implements OnInit {
	@select('country') country;
	@select('loadingCurrencies') loadingCurrencies;

	constructor(private currencyService: CurrencyCovertService) { }

	ngOnInit() {
		this.currencyService.loadCurrencies();
	}
}
