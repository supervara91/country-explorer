import { select } from '@angular-redux/store';
import { Component } from '@angular/core';

@Component({
	selector: 'description-component',
	templateUrl: './description.component.html',
	styleUrls: ['./description.component.css']
})
export class DescriptionComponent {
	@select('country') country;

	constructor() { }

}
