import { select } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services/news.service';

@Component({
	selector: 'news-details-component',
	templateUrl: './news-details.component.html',
	styleUrls: ['./news-details.component.css']
})
export class NewsDetailsComponent implements OnInit {
	@select('country') country;
	@select('news') news;
	@select('loadingNews') loadingNews;

	constructor(private newsService: NewsService) { }

	ngOnInit() {
		this.newsService.loadNews();
	}

}
