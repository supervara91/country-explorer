import { select } from '@angular-redux/store';
import { Component } from '@angular/core';

@Component({
	selector: 'description-details-component',
	templateUrl: './description-details.component.html',
	styleUrls: ['./description-details.component.css']
})
export class DescriptionDetailsComponent {
	@select('country') country;

	constructor() { }
}
