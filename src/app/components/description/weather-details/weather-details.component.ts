import { select } from '@angular-redux/store';
import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
	selector: 'weather-details-component',
	templateUrl: './weather-details.component.html',
	styleUrls: ['./weather-details.component.css']
})
export class WeatherDetailsComponent implements OnInit {
	@select('country') country;
	@select('weather') weather;
	@select('loadingWeather') loadingWeather;

	constructor(private weatherService: WeatherService) { }

	ngOnInit() {
		this.weatherService.loadWeather();
	}
}
